package com.ex.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ex.DAO.ReimbursementDAOImpl;
import com.ex.model.Reimbursement;
import com.google.gson.Gson;

/**
 * Servlet implementation class ReimbursementServlet
 */
public class ReimbursementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReimbursementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("------------------------\nPOST LOGIN METHOD CALLED\n------------------------");
		String command =request.getParameter("command");
		Map<String,Object> rObj= new HashMap();
		System.out.println("Reimbursement command : "+command);
		HttpSession s=request.getSession();
		try {
		switch(command){
		case "new":
			System.out.println("Reimbursement command : "+command);
			String uId 		=request.getParameter("uId");
			String type 	=request.getParameter("type");
			String amount 	=request.getParameter("amount");
			String receipt 	=request.getParameter("receipt");
			String comments	=request.getParameter("comments");
			
			//nt reimbursementId, String statusValue, int employeeId, Date dateSubmitted, int amount,
			//String receipt, String comments, int managerId, Date dateResolved, int resolvedAmount,
			//String reimbursementTypeName
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date dateobj = new Date();
			String fDate = df.format(dateobj);
			Date fDateObj = df.parse(fDate);
			System.out.println("DAO Done : "+ fDate);
			Reimbursement reim = new Reimbursement(0,
													"Pending",
													Integer.parseInt(uId),
													fDateObj,
													Integer.parseInt(amount),
													receipt,
													comments,
													0,
													null,
													0,
													type);
			System.out.println(reim.toString());
			ReimbursementDAOImpl rdao = new ReimbursementDAOImpl();
			boolean result =rdao.save(reim);
			
			if(result){
				System.out.println("Form Submitted succesfully");
				rObj.put("new", "true");
			} else{
				System.out.println("FAILED REimbursement DAO");
				rObj.put("new", "false");
			}
			break;
		
		case "profile":
			
			break;
		default:
			break;
		}
		
		
		String json = new Gson().toJson(rObj);
		System.out.println("RETUREn : "+json);
	    response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(json);
		
	}
		catch(Exception e){
			
		}
		
	}

}
