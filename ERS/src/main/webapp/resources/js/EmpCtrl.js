angular.module("EmpApp",[]).controller("EmpCtrl", function($scope, $http,$location) {

    $scope.curCmd="";
    $scope.reimburseOptions=[];
    $scope.Username="";
    $scope.Password="";
    $scope.newPwdConfirm="";
    $scope.newPwd="";
    $scope.Name="";
    $scope.UserId="";
    $scope.sOpt= "---Choose---";
    $scope.Amount="";
    $scope.Receipt="";
    $scope.Comments="";
    $scope.reimbursements;
    
    $scope.command= function(cmdName){
    	$scope.curCmd = cmdName;
    	console.log("cmd : "+cmdName);
    	
    	$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";
    	var cmdInfo = $.param({'command':cmdName})
    	switch(cmdName){
	    	case "new":
	    		$http({
    	            method : "POST",
    	            url : "EmployeeServlet",
    	            data: cmdInfo,
    	            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    	        }).success(function(response) {
		    		$scope.Name=response.user;
		    		$scope.UserId=response.userid;
		    		$scope.reimburseOptions=response.rTypeList;
    	        })
		    		angular.element(document.querySelector( '#form_new' )).css('display', 'block');
		    		angular.element(document.querySelector( '#form_all' )).css('display', 'none');
		    		angular.element(document.querySelector( '#form_user' )).css('display', 'none');
		    		break;
		    		
	    	case "user":
	    			$http({
	    	            method : "POST",
	    	            url : "EmployeeServlet",
	    	            data: cmdInfo,
	    	            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    	        }).success(function(response) {
	    	        	console.log("Emp : "+response.username);
	    	        	console.log("Empsd : "+response.password);
	    	        	
			    		$scope.Username=response.username;
			    		$scope.Password=response.password;
	    	        })
 
		    		angular.element(document.querySelector( '#form_user' )).css('display', 'block');
		    		angular.element(document.querySelector( '#form_all' )).css('display', 'none');
		    		angular.element(document.querySelector( '#form_new' )).css('display', 'none');
		    		break;
		    		
	    	case "all":
	    		$http({
    	            method : "POST",
    	            url : "EmployeeServlet",
    	            data: cmdInfo,
    	            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    	        }).success(function(response) {
		    		
		    		$scope.reimbursements=response.reimbursements;
    	        })

		    		angular.element(document.querySelector( '#form_all' )).css('display', 'block');
		    		angular.element(document.querySelector( '#form_user' )).css('display', 'none');
		    		angular.element(document.querySelector( '#form_new' )).css('display', 'none');
		    		break;
		    		
	    	default:
	    			angular.element(document.querySelector( '#form_default' )).css('display', 'block');
	    			break;
    	}
    	
    	angular.element(document.querySelector( '#result' )).css('display', 'block');
    }
    
    $scope.submitForm=function(){
    	angular.element(document.querySelector( '#form_new' )).css('display', 'none');
    	angular.element(document.querySelector( '#result' )).css('display', 'none');
    	console.log("Submit entered");
    	
       $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";
       var uId = $scope.UserId;
       var type = $scope.sOpt;
       var amount = $scope.Amount;
       var receipt = $scope.Receipt;
       var comments = $scope.Comments;
    	var cmdInfo = $.param({'command':"new", 'uId':uId, 'type':type, 'amount':amount, 'receipt':receipt, 'comments':comments})
    	$http({
            method : "POST",
            url : "ReimbursementServlet",
            data:cmdInfo,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {

        	if(response.new == "true"){
        		msg();
        		
        	}
        	
        	$scope.Amount="";
        	$scope.Receipt="";
        	$scope.Comments="";
        	$scope.sOpt= "---Choose---";
        })
    
    }
    
    $scope.submitProfile=function(){
    	angular.element(document.querySelector( '#form_user' )).css('display', 'none');
    	angular.element(document.querySelector( '#result' )).css('display', 'none');
    
    	 $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";
         var newPwd = $scope.newPwd;
         var newPwdConfirm = $scope.newPwdConfirm;
         var user = $scope.Username;
      	var cmdInfo = $.param({'command':"userProfile", 'newPwd':newPwd,'user':user})
      	$http({
              method : "POST",
              url : "EmployeeServlet",
              data:cmdInfo,
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response) {

          	if(response.profileUpdated == "true"){
          		msg1("Profile Updated Succesfully!!");
          	}else{
          		msg1("ERROR! Update Failed");
          	}
          })	
          
          $scope.newPwd="";
      	  $scope.newPwdConfirm="";
    
    }
    
    $scope.ViewForm=function(){
    	angular.element(document.querySelector( '#form_all' )).css('display', 'none');
    	angular.element(document.querySelector( '#result' )).css('display', 'none');
    }
    
   $scope.Cancel=function(){
	    angular.element(document.querySelector( '#form_user' )).css('display', 'none');
   		angular.element(document.querySelector( '#result' )).css('display', 'none');
   	 
   		$scope.newPwd="";
 	    $scope.newPwdConfirm="";
   }
   
   $scope.CancelN=function(){
	   angular.element(document.querySelector( '#form_new' )).css('display', 'none');
  		angular.element(document.querySelector( '#result' )).css('display', 'none');
  		
  		$scope.Amount="";
    	$scope.Receipt="";
    	$scope.Comments="";
    	$scope.sOpt= "---Choose---";
  }
});

function msg(){
	swal("Reimbursement","Submitted Succesfully!!");
	
}

function msg1(msg){
	swal(msg);
	
}
function myFunction() {
	  var input, filter, table, tr, td, i;
	  input = document.getElementById("myInput");
	  filter = input.value.toUpperCase();
	  table = document.getElementById("dev-table");
	  tr = table.getElementsByTagName("tr");
	  for (i = 0; i < tr.length; i++) {
	    td = tr[i].getElementsByTagName("td")[3];
	    if (td) {
	    	console.log(td);
	      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
	        tr[i].style.display = "";
	      } else {
	        tr[i].style.display = "none";
	      }
	    }       
	  }
	}
