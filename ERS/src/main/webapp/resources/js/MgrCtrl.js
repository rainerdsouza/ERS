angular.module("MgrApp",[]).controller("MgrCtrl", function($scope, $http,$location) {

    $scope.curCmd="";
    $scope.reimburseOptions=[];
    $scope.Username="";
    $scope.Password="";
    $scope.newPwdConfirm="";
    $scope.newPwd="";
    $scope.Name="";
    $scope.UserId="";
    $scope.sOpt= "---Choose---";
    $scope.Amount="";
    $scope.Receipt="";
    $scope.Comments="";
    $scope.reimbursements;
    $scope.reimbursementsPending;
    $scope.resolvedAmt=0;
    $scope.command= function(cmdName){
    	$scope.curCmd = cmdName;
    	console.log("cmd : "+cmdName);
    	
    	$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";
    	
    	var cmdInfo = $.param({'command':cmdName})
    	switch(cmdName){
	    	case "new":
		    		angular.element(document.querySelector( '#form_new' )).css('display', 'block');
		    		angular.element(document.querySelector( '#form_all' )).css('display', 'none');
		    		angular.element(document.querySelector( '#form_pending' )).css('display', 'none');
		    		break;
		    		
	    	case "all":
	    		$http({
    	            method : "POST",
    	            url : "ManagerServlet",
    	            data: cmdInfo,
    	            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    	        }).success(function(response) {
		    		
		    		$scope.reimbursements=response.reimbursements;
    	        })

		    		angular.element(document.querySelector( '#form_all' )).css('display', 'block');
		    		angular.element(document.querySelector( '#form_pending' )).css('display', 'none');
		    		angular.element(document.querySelector( '#form_new' )).css('display', 'none');
		    		break;
		    		
	    	case "viewPending":
	    		$http({
    	            method : "POST",
    	            url : "ManagerServlet",
    	            data: cmdInfo,
    	            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    	        }).success(function(response) {
		    		
		    		$scope.reimbursementsPending=response.reimbursementsPending;
    	        })

	    		angular.element(document.querySelector( '#form_pending' )).css('display', 'block');
	    		angular.element(document.querySelector( '#form_all' )).css('display', 'none');
	    		angular.element(document.querySelector( '#form_new' )).css('display', 'none');
	    		break;
		    		
	    	default:
	    			angular.element(document.querySelector( '#form_default' )).css('display', 'block');
	    			break;
    	}
    	
    	angular.element(document.querySelector( '#result' )).css('display', 'block');
    }
    
    $scope.reimbursementObjsToSave = [];
    $scope.SaveAll=function(){
    	angular.element(document.querySelector( '#form_pending' )).css('display', 'none');
    	angular.element(document.querySelector( '#form_default' )).css('display', 'none');
    	
    	$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";
    	
    	for(var j=0;j<$scope.reimbursementObjsToSave.length;j++)
    	{
    		var status = $scope.reimbursementObjsToSave[j].status;
    		var rid    = $scope.reimbursementObjsToSave[j].rid; 
    		var amt    = $scope.reimbursementObjsToSave[j].rAmt; 
    		console.log("Processing : "+amt);
    		var cmdInfo = $.param({'command':"SaveAll","status":status,"rid":rid,"rAmt":amt});
	    	$http({
	            method : "POST",
	            url : "ManagerServlet",
	            data: cmdInfo,
	            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	        }).success(function(response) {
	    		
	    		;
	        })
    	}
    }
    
    $scope.approve=function(reimbursementId){
    	console.log("Curr RAmt : "+$scope.resolvedAmount);
    	for(var i=0;i<$scope.reimbursementsPending.length;i++){
    		if($scope.reimbursementsPending[i].reimbursementId==reimbursementId){
    			$scope.reimbursementsPending[i].statusValue="Approved";
    			var obj = {"rid":$scope.reimbursementsPending[i].reimbursementId,
 					   "status":$scope.reimbursementsPending[i].statusValue,
 					   "rAmt":$scope.reimbursementsPending[i].resolvedAmount
 					   };
	 			if($scope.reimbursementObjsToSave.indexOf(obj) < 0){
	 				$scope.reimbursementObjsToSave.push(obj);
	 			}
	 			
    			break;
    		}
    	}
    }
    
    $scope.decline=function(reimbursementId){
    	for(var i=0;i<$scope.reimbursementsPending.length;i++){
    		if($scope.reimbursementsPending[i].reimbursementId==reimbursementId){
    			$scope.reimbursementsPending[i].statusValue="Declined";
    			var obj = {"rid":$scope.reimbursementsPending[i].reimbursementId,
    					   "status":$scope.reimbursementsPending[i].statusValue,
    					   "rAmt":$scope.reimbursementsPending[i].resolvedAmount
    					   };
    			if($scope.reimbursementObjsToSave.indexOf(obj) < 0){
    				$scope.reimbursementObjsToSave.push(obj);
    			}
    			
    			break;
    		}
    	}
    }
    
    $scope.submitForm=function(){
    	angular.element(document.querySelector( '#form_new' )).css('display', 'none');
    	angular.element(document.querySelector( '#result' )).css('display', 'none');
    	
    	var newFirstName = $scope.PersonFirstName;
    	var newLastName = $scope.PersonLastName;
    	var newUserName = $scope.PersonUserName;
    	var newPassword = $scope.PersonPassword;
    	var cmdInfo = $.param({'command':"new", 'newFirstName':newFirstName, 'newLastName':newLastName, 'newUserName':newUserName, 'newPassword':newPassword})
    	$http({
    	            method : "POST",
    	            url : "ManagerServlet",
    	            data: cmdInfo,
    	            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    	        }).success(function(response) {
    	        	if(response.profileCreated == "true"){
    	          		msg1("Profile Created Succesfully!!");
    	          	}else{
    	          		msg1("ERROR! Profile Created Failed");
    	          	}
    	        })
    	        
    	        $scope.PersonFirstName="";
    			$scope.PersonLastName="";
    			$scope.PersonUserName="";
    			$scope.PersonPassword="";
    }
   
});

function msg(){
	swal("Reimbursement","Submitted Succesfully!!");
	
}

function msg1(msg){
	swal(msg);
	
}
function myFunction() {
	  var input, filter, table, tr, td, i;
	  input = document.getElementById("myInput");
	  filter = input.value.toUpperCase();
	  table = document.getElementById("dev-table");
	  tr = table.getElementsByTagName("tr");
	  for (i = 0; i < tr.length; i++) {
	    td = tr[i].getElementsByTagName("td")[6];
	    if (td) {
	    	console.log(td);
	      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
	        tr[i].style.display = "";
	      } else {
	        tr[i].style.display = "none";
	      }
	    }       
	  }
}
	  function myFunctionA() {
		  var input, filter, table, tr, td, i;
		  input = document.getElementById("myInputA");
		  filter = input.value.toUpperCase();
		  table = document.getElementById("dev-tableA");
		  tr = table.getElementsByTagName("tr");
		  for (i = 0; i < tr.length; i++) {
		    td = tr[i].getElementsByTagName("td")[6];
		    if (td) {
		    	console.log(td);
		      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
		        tr[i].style.display = "";
		      } else {
		        tr[i].style.display = "none";
		      }
		    }       
		  }
	}

