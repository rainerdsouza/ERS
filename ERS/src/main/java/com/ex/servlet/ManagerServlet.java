package com.ex.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ex.DAO.EmployeeDAOImpl;
import com.ex.DAO.ReimbursementDAOImpl;
import com.ex.model.Employees;
import com.ex.model.Reimbursement;
import com.ex.model.ReimbursementType;
import com.google.gson.Gson;

/**
 * Servlet implementation class ManagerServlet
 */
public class ManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ManagerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		HttpSession s = request.getSession();
		Map<String,Object> rObj= new HashMap();
		
		String command =request.getParameter("command");
		System.out.println("Command : "+command);
		switch (command) {
		case "new":
			 String newFirstName = request.getParameter("newFirstName");
			 String newLastName = request.getParameter("newLastName");
			 String newUserName = request.getParameter("newUserName");
			 String newPassword = request.getParameter("newPassword");
			 int roleId = 2;
			Employees e = new Employees(roleId, newFirstName, newLastName, newUserName, newPassword ); 
			boolean result = new EmployeeDAOImpl().save(e);
			
			if(result == true){
				rObj.put("profileCreated", "true");
			}
			else{
				rObj.put("profileCreated", "false");	
			}
			break;

		case "all":
			System.out.println("** ALL USERS REINBURSEMENTS ** ");
			int id = Integer.parseInt(s.getAttribute("userid").toString());
			List<Reimbursement> reimbursementList = new ReimbursementDAOImpl().findReimbursementbyManagerId(id);
			int cnt1=0;
			ArrayList<Reimbursement> reimb = new ArrayList();
			for(Reimbursement r: reimbursementList){
				cnt1++;
				reimb.add(r);
			}
			rObj.put("reimbursements", reimb);
			System.out.println("No. of Reimbursements are All : "+cnt1);
			break;
			
		case "viewPending":
			
			List<Reimbursement> reimbursementObjs = new ReimbursementDAOImpl().findReimbursementbyStatusId("Pending");
			int cnt=0;
			ArrayList<Reimbursement> rImb = new ArrayList();
			for(Reimbursement r: reimbursementObjs){
				cnt++;
				rImb.add(r);
			}
			rObj.put("reimbursementsPending", rImb);
			System.out.println("No. of Reimbursements Pending : "+cnt);
			
			break;
		case "SaveAll":
			System.out.println("Save all");
			String status = request.getParameter("status");
			int rid = Integer.parseInt(request.getParameter("rid"));
			int rAmt = Integer.parseInt(request.getParameter("rAmt"));
			int managerid = Integer.parseInt(s.getAttribute("userid").toString());
			System.out.println(rAmt);
			new ReimbursementDAOImpl().update(status, rid, rAmt,managerid);
			break;
		}
		
		rObj.put("username", s.getAttribute("username"));
		rObj.put("password", s.getAttribute("password"));
		
		String json = new Gson().toJson(rObj);
		System.out.println("RETUREn : "+json);
	    response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(json);
	}

}
